<?php include './global/head.php'; ?>
<?php include './global/navigation.php'; ?>
<div class="row">
    <div class="columns">
        <div class="container">
            <h1>PROFILO</h1>
            <p>Shane Marcel è Piastrellista con Attestato Federale di Capacità, indipendente.La sua attività principale è la posa di piastrelle, mosaici, pietre naturali e pietre artificiali. Esegue ogni tipo di posa, secondo schemi classici o schemi moderni, con formati di qualsiasi tipo, all’interno così come all’esterno.</p>
            <p>Al cliente viene garantito:</p>

            <ul>
                <li>un servizio accurato</li>
                <li>il rispetto dei termin</li>
                <li>la precisione nell’esecuzione</li>
                <li>l’utilizzo dei migliori prodotti presenti sul mercato adatti ad ogni esigenza</li>
                <li>l’aggiornamento continuo alle nuove tecnologie e ai nuovi materiali</li>
            </ul>

            <h2>La Posa</h2>
            <p>La posa è il procedimento mediante il quale le piastrelle, i mosaici, le pietre naturali, ecc. vengono fissate. E’importante che la posa sia eseguita con precisione: se ne avranno benefici sia dal punto di vista estetico che funzionale. La riuscita della posa è dovuta non solo alla qualità delle piastrelle, ma a tutto un insieme di elementi (sottofondo, strato legante, giunti di dilatazione, piastrelle, ecc.) che costituiscono un vero e proprio sistema. Ecco perché le operazioni di posa sono da considerarsi importanti tanto quanto la scelta delle piastrelle. In questa fase entra in gioco la capacità di saper combinare estetica e tecnica. La progettazione è una fase preliminare indispensabile alla realizzazione di una superficie correttamente rivestita. Per elaborare un buon progetto è necessario analizzare le caratteristiche della superficie da rivestire. A questa prima fase di valutazione segue un esame incrociato delle proprietà delle piastrelle, dell’ambiente di destinazione e delle caratteristiche del supporto da piastrellare. La durabilità del pavimentoe della parete così rivestiti dipende anche da un adeguato uso e una corretta manutenzione e pulizia.</p>
        </div>
    </div>
</div>
<?php include './global/footer.php'; ?>
