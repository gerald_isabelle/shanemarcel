<?php include './global/head.php'; ?>
<?php include './global/navigation.php'; ?>
<div class="row">
    <div class="columns">
        <div class="container">
            <h1>SERVIZI</h1>
            <h3>Consulenza</h3>
            <p>Shane Marcel vi accompagna nella scelta dei materiali, cercando di consigliare al meglio e ponendo al centro dell’attenzione le esigenze ed i desideri del cliente.</p>

            <h3>Vendita</h3>
            <p>Grazie all’appoggio su ditte specializzate nella vendita dei materiali, viene offerto un servizio a 360 gradi di alta qualità e a prezzi interessanti.</p>

            <h3>Posa</h3>
            <p>Esecuzione di ogni tipo di posa, dai schemi più semplici e classici a schemi complessi, con formati di qualsiasi tipo, all’interno così come all’esterno di:</p>
            <ul>
                <li>piastrelle di tutte le tipologie.</li>
                <li>mosaici (anche per arredi).</li>
                <li>pietre naturali e pietre artificiali.</li>
                <li>Decori e fantasia:</li>
            </ul>
            <p>Una delle nuove tendenze consiste nell’applicazione della piastrella o altri materiali (rasante decorativo, pietre composte, foglie di pietra, ecc.) fuori dall’applicazione usuale. Viene offerta una vasta scelta di materiali nuovi per i quali è stata seguita un’adeguata formazione affinché vengano impiegati a regola d’arte in modo da poter dare la garanzia e la sicurezza sull’operato.</p>

            <h3>Riattazioni - Ristrutturazioni</h3>
            <p>Nell’ambito di una riattazione/ristrutturazione vengono eseguiti tutti i lavori necessari alla preparazione del fondo su cui verrà effettuata la nuova posa, compresa la demolizione, l’estrazione dei vecchi materiali nonché il loro smaltimento (vecchie piastrelle, parquet, moquette e scarti vari).</p>

            <h3>Riparazioni</h3>
            <p>Grazie ad un’attrezzatura adeguata e sistemi innovativi si evitano disagi come ad esempio la grande emissione di polvere, cosa fondamentale  quando si interviene nelle abitazioni.</p>

            <h3>Pulizia e manutenzione</h3>
            <p>Problemi di salnitro, muffe, fughe scolorite, macchiate o molto sporche, vengono risolti senza costi esorbitanti e senza troppi disagi.</p>
            <p>Per la pulizia vengono utilizzati prodotti adatti ad ogni tipo di problema nonché prodotti mirati alla prevenzione. </p>
        </div>
    </div>
</div>


<?php include './global/footer.php'; ?>
