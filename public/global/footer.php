<footer>
    <div class="row">
        <div class="columns">
            <div class="container">
                <p class="text-right">©2015 Shanemarcel.ch</p>
            </div>
        </div>
    </div>
</footer>

<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<script src="../bower_components/foundation/js/foundation.min.js"></script>
<script src="../js/app.js"></script>
</body>
</html>