<div class="nav-wrap">
    <div class="row">
        <div class="columns">
            <div class="container">
                <nav class="top-bar" data-topbar role="navigation">
                    <ul class="title-area">
                        <li class="name">
                            <a href="#"><img src="img/logo.png"></a>
                        </li>
                        <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
                        <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
                    </ul>

                    <section class="top-bar-section">
                        <!-- Right Nav Section -->
                        <ul class="right main-nav">
                            <li class="active"><a href="index2.php">home</a></li>
                            <li class=""><a href="profilo.php">profilo</a></li>
                            <li class=""><a href="servizi.php">servizi</a></li>
                            <li class="has-dropdown">
                                <a href="#">galleria</a>
                                <ul class="dropdown">
                                    <li><a href="pavimenti.php">pavimenti</a></li>
                                    <li class=""><a href="pareti.php">pareti</a></li>
                                    <li class="active"><a href="scale.php">scale</a></li>
                                </ul>
                            </li>
                            <li class=""><a href="contatti.php">contatti</a></li>
                        </ul>
                    </section>
                </nav>
            </div>
        </div>
    </div>
</div>