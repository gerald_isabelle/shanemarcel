<?php include './global/head.php'; ?>
<?php include './global/navigation.php'; ?>

<ul class="example-orbit" data-orbit>
    <li>
        <img src="./img/decor/1.jpg" alt="slide 1" />
        <div class="orbit-caption">
            image One.
        </div>
    </li>
    <li class="active">
        <img src="./img/decor/2.jpg" alt="slide 2" />
        <div class="orbit-caption">
            image Two.
        </div>
    </li>
    <li>
        <img src="./img/decor/3.jpg" alt="slide 3" />
        <div class="orbit-caption">
            image Three.
        </div>
    </li>
</ul>
<div class="row">
    <div class="columns">
        <h1 class="hard--top flush--top">Il cliente va aiutato a realizzare il proprio sogno</h1>
        <p>Shane Marcel ricerca la piena soddisfazione del cliente ed è per questo che lo segue nel percorso che porta alla realizzazione del suo progetto partendo dalla scelta dei materiali più idonei nelle forme e nei colori sviluppando soluzioni di posa che tengano conto delle esigenze e dei desideri del cliente.</p>
        <p>Questo percorso è molto importante perché porterà alla realizzazione di ambient/spazi che faranno parte della vita quotdiana del cliente dando una sensazione di benessere.</p>
    </div>
</div>

<?php include './global/footer.php'; ?>
